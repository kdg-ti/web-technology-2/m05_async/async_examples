import {timeLog} from "./timer.js";

export interface Person {
	id: number;
	born: string;
	name: string;
	mother: number;
}

const pedigree: Person[] = [
	{id: 1, born: '1972', name: 'Maud', mother: 2},
	{id: 2, born: '1945', name: 'Erika', mother: 3},
	{id: 3, born: '1925', name: 'Goedele', mother: 0},
	{id: 4, born: '1949', name: 'Electra', mother: 3},
	{id: 5, born: '1966', name: 'Ruth', mother: 2},
	{id: 6, born: '1993', name: 'Lilith', mother: 5},
	{id: 7, born: '1995', name: 'Aicha', mother: 5},
];

export function getPerson(id: number) {
	return pedigree.find((person: Person) => person.id === id);
}

export function dbLog(id: number, result?: Person | string): void {
	timeLog(result ?? "Invalid ID: " + id);
}