import * as prom from "../repo/dbSearchPromise.js";
import {dbLog} from "../Person.js";
import {timeLog} from "../timer.js";

console.log("--- Start search grandmother using promises ---");

prom.dbSearch(1)
	.then(person => {return prom.dbSearch(person.mother)})
	.then(mother => prom.dbSearch(mother.mother))
	.then(granny => dbLog(granny.id,granny))
	.catch(err => timeLog("Caught " + err.message));

console.log("--- End search grandmother using promises ---");
