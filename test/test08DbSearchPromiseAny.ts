import * as prom  from "../repo/dbSearchPromise.js"
import {timeLog} from "../timer.js";

Promise.any([prom.dbSearch(1), prom.dbSearch(2), prom.dbSearch(3)])
    .then(timeLog)
    .catch(timeLog);
