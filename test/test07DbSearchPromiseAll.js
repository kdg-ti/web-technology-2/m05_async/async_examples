"use strict";
exports.__esModule = true;
var dbSearchPromise_1 = require("./dbSearchPromise");
Promise.all([(0, dbSearchPromise_1["default"])(1), (0, dbSearchPromise_1["default"])(2), (0, dbSearchPromise_1["default"])(3)])
    .then(dbSearchPromise_1.timeLog)["catch"](dbSearchPromise_1.timeLog);
