"use strict";
exports.__esModule = true;
var dbSearchPromise_1 = require("./dbSearchPromise");
var ra = [];
(0, dbSearchPromise_1["default"])(1)
    .then(function (result) {
    ra.push(result);
    return (0, dbSearchPromise_1["default"])(2);
})
    .then(function (result) {
    ra.push(result);
    return (0, dbSearchPromise_1["default"])(3);
})
    .then(function (result) {
    ra.push(result);
    (0, dbSearchPromise_1.timeLog)(ra);
})["catch"](function (error) { return (0, dbSearchPromise_1.timeLog)(error); });
