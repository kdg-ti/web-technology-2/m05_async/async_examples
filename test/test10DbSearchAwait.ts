import * as prom from "../repo/dbSearchPromise.js"
import * as sync from "../repo/dbSearchSync.js"
import {Person} from "../Person.js";
import {resetTime, timeLog} from "../timer.js";

//gewone (synchrone) dbSearch functie
function searchSync() {
	let p: Person = sync.dbSearch(1);
	timeLog({type:"sync",person: p});
	p = sync.dbSearch(p.mother);
	timeLog({type:"sync",person: p});
	p = sync.dbSearch(p.mother);
	timeLog({type:"sync",person: p});
}

// gebruik van Promises
function searchPromise() {
	prom.dbSearch(1)
		.then(p => {
			timeLog({type:"prom",person: p});
			return prom.dbSearch(p.mother)
		}).then(p => {
			timeLog({type:"prom",person: p});
			return prom.dbSearch(p.mother)
	}).then(p =>
		timeLog({type:"prom",person: p}));
}

// // gebruik van async/await
// // roep 3 x de asynchnrone dbSearch aan
// // log de versrtreken tijd
async function searchAsync() {
	let p = await prom.dbSearch(1);
	timeLog({type:"await",person: p});
	p = await prom.dbSearch(p.mother);
	timeLog({type:"await",person: p});
	p = await prom.dbSearch(p.mother);
	timeLog({type:"await",person: p});
}

// gebruik van Promises met error handling (.then/.catch)
function searchError() {
	prom.dbSearch(1)
		.then(p => {
			timeLog({type:"then",person:p});
			return prom.dbSearch(p.id - 10)
		})
		.then(p => {
			timeLog({type:"then",person:p});
			return prom.dbSearch(2)
		})
		.then(p=> ({type:"then",person:p}))
		.catch(r => timeLog("catch " + r));
}


// gebruik van async/await met error handling (try/catch)
// alle functies binnen de catch
async function searchErrorAwait() {
	try {
		let p = await prom.dbSearch(1);
		timeLog({type:"try",person:p});
		p = await prom.dbSearch(p.id - 10);
		timeLog({type:"try",person:p});
		p = await prom.dbSearch(2);
		timeLog({type:"try",person:p});
	} catch (r) {
		timeLog("try catch " + r);
	}
}


// aanroepen van te testen functies
// aangezien de functies asynchroon zijn kunnen de resultaten van de verschillende functies
// door elkaar getoond worden als je er meerdere uitcommentatieert

// console.log("Start search synchronous");
// searchSync();
//
// console.log("Start search asynchronous with promises");
// searchPromise();
//
// console.log("Start search asynchronous with await");
// searchAsync();
// timeLog({type:"Top level await",person:await prom.dbSearch(1)});
// console.log("I'm after top level await is finished!");

// console.log("start erroneous search with promises and error");
// searchError()
// console.log("start erroneous search with async/await and error");
// searchErrorAwait()


