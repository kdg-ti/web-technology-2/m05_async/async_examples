
import * as callback from "../repo/dbSearchCallback.js"

import {dbLog, Person} from "../Person.js";
import {resetTime, timeLog} from "../timer.js";


console.log("--- Start asynchrone callback search in the database ---");
function printResult(error?: Error , person?: Person|string) {
	timeLog(error?.message ??  person);
}
callback.dbSearch(1,
	(err,person) => printResult(err,person));
callback.dbSearch(2,printResult);
callback.dbSearch(-1,printResult);
console.log("--- End asynchrone callback in the database ---");