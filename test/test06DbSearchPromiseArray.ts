import  * as prom from "../repo/dbSearchPromise.js";
import {Person} from "../Person.js";
import {timeLog} from "../timer.js";

let ra: Person[] = [];

prom.dbSearch(1)
    .then((result: any) => {
        ra.push(result);
        return prom.dbSearch(2);
    })
    .then((result: any) => {
        ra.push(result);
        return prom.dbSearch(3);
    })
    .then((result: any) => {
        ra.push(result);
        timeLog(ra);
    })
    .catch((error: any) => timeLog(error));
