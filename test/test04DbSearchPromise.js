"use strict";
exports.__esModule = true;
var dbSearchPromise_1 = require("./dbSearchPromise");
console.log("\tLooking up person with id", 2);
(0, dbSearchPromise_1["default"])(1)
    .then(function (result) {
    (0, dbSearchPromise_1.timeLog)(result);
    console.log("\tNow looking up mother of id", result.id);
    return (0, dbSearchPromise_1["default"])(result.mother);
})
    .then(function (result) {
    (0, dbSearchPromise_1.timeLog)(result);
    console.log("\tNow looking up mother of id", result.id);
    return (0, dbSearchPromise_1["default"])(result.mother);
})
    .then(dbSearchPromise_1.timeLog)["catch"](function (err) { return (0, dbSearchPromise_1.timeLog)("Caught " + err.message); });
console.log("That's all folks!");
