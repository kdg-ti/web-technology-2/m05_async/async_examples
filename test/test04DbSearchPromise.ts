import * as prom from "../repo/dbSearchPromise.js"
import {timeLog} from "../timer.js";




console.log("--- Start asynchrone promise search in the database ---");
prom.dbSearch(1)
		.then(person => timeLog(person));
prom.dbSearch(2)
	.then(timeLog)
	.catch(err => timeLog("Caught " + err.message));
prom.dbSearch(-1)
	.then(timeLog)
	.catch(err => timeLog("Caught " + err.message));
console.log("--- end asynchrone promise search in the database ---");


