import * as sync from "../repo/dbSearchSync.js"
import * as async from "../repo/dbSearchAsync.js"

import {dbLog} from "../Person.js";
import {resetTime} from "../timer.js";

console.log("--- Start synchronous search in the database ---");
dbLog(1,sync.dbSearch(1));
dbLog(2,sync.dbSearch(2));
dbLog(-1,sync.dbSearch(-1));
console.log("--- End synchronous search in the database ---");

resetTime();
console.log("--- Start asynchronous search in the database ---");
dbLog(1,async.dbSearch(1));
dbLog(2,async.dbSearch(2));
console.log("--- End asynchronous search in the database ---");

