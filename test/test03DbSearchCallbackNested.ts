import * as callback from "../repo/dbSearchCallback.js"

import {dbLog, Person} from "../Person.js";
import {resetTime, timeLog} from "../timer.js";


function printResult(error?: Error, result?: any) {
	timeLog(error?.message ?? result);
}


console.log("--- Start search grandmother using callbacks ---");
callback.dbSearch(1, (error, person) => {
	if (person?.mother) {
		callback.dbSearch(person.mother, (error, mother) => {
			if (mother?.mother) {
				callback.dbSearch(mother.mother, (error, granny) =>
					printResult(error, {result: "Granny of 1" , person: granny})
				);
			}
		});
	}
});
console.log("--- End search grandmother using callbacks ---");


