import * as prom  from "../repo/dbSearchPromise.js"
import {timeLog} from "../timer.js";

Promise.allSettled([prom.dbSearch(1),prom.dbSearch(2),prom.dbSearch(-1)])
	.then(timeLog)
	.catch(timeLog);
