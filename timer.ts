let start: number;

// start timer is done when loading the module for simplicity
// but you can reset it
resetTime();

export function resetTime() {
	start = Date.now();
}

/**
 * Turn back the time :0
 * @param rewind
 */
export function fakeElapse(rewind: number) {
	start -= rewind;
}

export function elapsed() {
	return Math.round(Date.now() - start);
}

export function timeLog(result: any): void {
	console.log(elapsed().toString().padStart(5), "ms:", JSON.stringify(result))
}