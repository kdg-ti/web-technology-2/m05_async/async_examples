"use strict";
/** Created by Jan de Rijke & converted to Typescript by Koray Sels */
exports.__esModule = true;
exports.timeLog = void 0;
// start timer is done when loading the module for simplicity
// a good implementation would have a start method to be called by the user of the module
var start = Date.now();
var pedigree = [{ id: 1, born: '1972', name: 'Maud', mother: 2 },
    { id: 2, born: '1945', name: 'Erika', mother: 3 },
    { id: 3, born: '1925', name: 'Goedele', mother: 0 },
    { id: 4, born: '1949', name: 'Electra', mother: 3 },
    { id: 5, born: '1966', name: 'Goedele', mother: 2 }];
function getPerson(id) {
    return pedigree.find(function (person) { return person.id === id; });
}
function timeLog(result) {
    console.log(Date.now() - start, "ms:", JSON.stringify(result));
}
exports.timeLog = timeLog;
function dbLog(id, result) {
    timeLog(result ? result : "Invalid ID: " + id);
}
function dbSearchSync(id) {
    return getPerson(id);
}
exports["default"] = dbSearchSync;
