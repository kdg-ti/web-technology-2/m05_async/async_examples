import timers from "timers/promises"
import {getPerson, Person} from "../Person.js";

export  function dbSearch(id: number): Promise<Person> {
    return timers
        .setTimeout(Math.random() * 1000, getPerson(id))
        .then(result => result ??
	        Promise.reject(new Error("Reject ID: " + id)))
}


