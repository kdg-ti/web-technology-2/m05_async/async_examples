import {Person, getPerson} from "../Person.js";


export function dbSearch(id: number,callback: (error?: Error, p?: Person) => void ) {
	setTimeout(() =>searchAndCallback(id,callback) ,
		Math.random() * 1000);
}

function searchAndCallback( id: number,callback: (error?: Error, p?: Person) => void){
	const person = getPerson(id);
	callback(person ? undefined: new Error(`Person ${id} not found`) , person);
}



