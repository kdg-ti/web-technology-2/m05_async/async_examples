"use strict";
exports.__esModule = true;
exports.timeLog = void 0;
/** Created by Jan de Rijke & converted to TypeScript By Koray Sels*/
var promises_1 = require("timers/promises");
// start timer is done when loading the module for simplicity
// a good implementation would have a start method to be called by the user of the module
var start = Date.now();
var pedigree = [{ id: 1, born: '1972', name: 'Maud', mother: 2 },
    { id: 2, born: '1945', name: 'Erika', mother: 3 },
    { id: 3, born: '1925', name: 'Goedele', mother: 0 },
    { id: 4, born: '1949', name: 'Electra', mother: 3 },
    { id: 5, born: '1966', name: 'Goedele', mother: 2 }];
function getPerson(id) {
    return pedigree.find(function (product) { return product.id === id; });
}
// test promises
function dbSearchPromise(id) {
    return promises_1["default"]
        .setTimeout(Math.random() * 1000, getPerson(id))
        .then(function (result) { return result !== null && result !== void 0 ? result : Promise.reject(new Error("Reject ID: " + id)); });
}
exports["default"] = dbSearchPromise;
function timeLog(result) {
    console.log(Date.now() - start, "ms:", JSON.stringify(result));
}
exports.timeLog = timeLog;
