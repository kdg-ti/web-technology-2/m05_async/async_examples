import {dbLog, getPerson, Person} from "../Person.js";


function printPerson(id: number) {
	const person = getPerson(id);
	console.warn("Log from within async search function!");
	dbLog(id, person);
	return person;
}

export function dbSearch( id: number) {
	let person:Person | undefined;
	setTimeout(() =>person=printPerson(id) ,
		Math.random() * 1000);
	return person;
}

