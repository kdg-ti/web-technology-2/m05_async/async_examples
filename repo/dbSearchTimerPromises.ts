import {getPerson,dbLog} from "../Person.js";
import timers from "timers/promises"




export function dbSearch(id: number): void {
    timers.setTimeout(Math.random() * 1000, getPerson(id))
        .then(result => dbLog(id, result))
        .catch((err: Error) => console.error(err));
}

//dbSearch(2);
