import {getPerson, Person} from "../Person.js";
import {fakeElapse} from "../timer.js";



export  function dbSearch(id: number): Person {
	fakeElapse(Math.random() * 1000);
	return getPerson(id)!;
}
